<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%maze}}".
 *
 * @property integer $id
 * @property string $public_id
 * @property integer $private
 * @property integer $maze_w
 * @property integer $maze_h
 * @property integer $tile_w
 * @property integer $tile_h
 * @property integer $start_x
 * @property integer $start_y
 * @property integer $end_x
 * @property integer $end_y
 * @property integer $last_generated
 */
class Maze extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%maze}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['public_id'], 'required'],
            [['private', 'maze_w', 'maze_h', 'tile_w', 'tile_h', 'start_x', 'start_y', 'end_x', 'end_y', 'last_generated'], 'integer'],
            [['public_id'], 'string', 'max' => 16],
            [['public_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'public_id' => 'Public ID',
            'private' => 'Private',
            'maze_w' => 'Maze W',
            'maze_h' => 'Maze H',
            'tile_w' => 'Tile W',
            'tile_h' => 'Tile H',
            'start_x' => 'Start X',
            'start_y' => 'Start Y',
            'end_x' => 'End X',
            'end_y' => 'End Y',
            'last_generated' => 'Last Generated',
        ];
    }
}
