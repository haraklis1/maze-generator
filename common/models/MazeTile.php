<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%maze_tile}}".
 *
 * @property integer $id
 * @property integer $maze_id
 * @property string $data
 */
class MazeTile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%maze_tile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['maze_id'], 'integer'],
            [['data'], 'string'],
            [['maze_id'], 'exist', 'skipOnError' => true, 'targetClass' => Maze::className(), 'targetAttribute' => ['maze_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'maze_id' => 'Maze ID',
            'data' => 'Data',
        ];
    }
}
