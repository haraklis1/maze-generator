<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%maze_data}}".
 *
 * @property integer $id
 * @property integer $maze_id
 * @property integer $x
 * @property integer $y
 * @property integer $maze_tile_id
 */
class MazeData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%maze_data}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['maze_id', 'x', 'y', 'maze_tile_id'], 'integer'],
            [['maze_id'], 'exist', 'skipOnError' => true, 'targetClass' => Maze::className(), 'targetAttribute' => ['maze_id' => 'id']],
            [['maze_tile_id'], 'exist', 'skipOnError' => true, 'targetClass' => MazeTile::className(), 'targetAttribute' => ['maze_tile_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'maze_id' => 'Maze ID',
            'x' => 'X',
            'y' => 'Y',
            'maze_tile_id' => 'Maze Tile ID',
        ];
    }
}
