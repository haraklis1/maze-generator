<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m161005_182251_init_maze_structure extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%maze}}', [
            'id' => $this->primaryKey(),
            'public_id' => $this->string(16)->notNull()->unique(),
            'private' => $this->boolean(),
            'maze_w' => $this->integer(),
            'maze_h' => $this->integer(),
            'tile_w' => $this->integer(),
            'tile_h' => $this->integer(),
            'start_x' => $this->integer(),
            'start_y' => $this->integer(),
            'end_x' => $this->integer(),
            'end_y' => $this->integer(),
            'last_generated' => $this->integer(),
        ], $tableOptions);


        $this->createTable('{{%maze_tile}}', [
            'id' => $this->primaryKey(),
            'maze_id' => $this->integer(),
            'data' => $this->text(),
        ], $tableOptions);

        $this->createIndex("maze_idx", "{{%maze_tile}}", "maze_id");
        $this->addForeignKey(
            "fk_maze_tile_maze",
            "{{%maze_tile}}",
            "maze_id",
            "{{%maze}}",
            "id",
            "CASCADE",
            "CASCADE"
        );

        $this->createTable('{{%maze_data}}', [
            'id' => $this->primaryKey(),
            'maze_id' => $this->integer(),
            'x' => $this->integer(),
            'y' => $this->integer(),
            'maze_tile_id' => $this->integer(),
        ], $tableOptions);

        $this->createIndex("maze_idx", "{{%maze_data}}", "maze_id");
        $this->addForeignKey(
            "fk_maze_data_maze",
            "{{%maze_data}}",
            "maze_id",
            "{{%maze}}",
            "id",
            "CASCADE",
            "CASCADE"
        );

        $this->createIndex("maze_tile_idx", "{{%maze_data}}", "maze_tile_id");
        $this->addForeignKey(
            "fk_maze_data_maze_tile",
            "{{%maze_data}}",
            "maze_tile_id",
            "{{%maze_tile}}",
            "id",
            "CASCADE",
            "CASCADE"
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            "fk_maze_tile_maze",
            "{{%maze_tile}}"
        );
        $this->dropIndex("maze_idx", "{{%maze_tile}}");


        $this->dropForeignKey(
            "fk_maze_data_maze",
            "{{%maze_data}}"
        );
        $this->dropIndex("maze_idx", "{{%maze_data}}");

        $this->dropForeignKey(
            "fk_maze_data_maze_tile",
            "{{%maze_data}}"
        );
        $this->dropIndex("maze_tile_idx", "{{%maze_data}}");

        $this->dropTable('{{%maze_data}}');


        $this->dropTable('{{%maze_tile}}');


        $this->dropTable('{{%maze}}');
    }
}
