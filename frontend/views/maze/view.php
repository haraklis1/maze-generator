<?php

/* @var $this yii\web\View */

$this->title = 'Maze';
?>

<section
    ng-app="mzntr"
    ng-controller="MazeController"
    public-id="<?= $publicId ?>"
>

    <div class="page-header">
        <h2>Maze</h2>
    </div>


    <div class="form-inline">
        <div class="form-group">
            <label for="newWidth">Width</label>
            <input type="number" class="form-control" id="newWidth" placeholder="Width" ng-model="newWidth">
        </div>
        <div class="form-group">
            <label for="newHeight">Height</label>
            <input type="number" class="form-control" id="newHeight" placeholder="Height" ng-model="newHeight">
        </div>
        <br>
        <!--        <button type="submit" class="btn btn-primary" ng-click="redrawMaze();">Redraw</button>-->
        <button ng-show="state != states.generating" type="submit" class="btn btn-primary"
                ng-click="generateMaze(true);">Generate
        </button>
        <button ng-show="state == states.success" type="submit" class="btn btn-success" ng-click="drawSolution();">
            Solve
        </button>
        <button ng-show="state == states.success" type="submit" class="btn btn-info" ng-click="export();">
            Export
        </button>
        <button ng-show="state == states.generating" type="submit" class="btn btn-danger"
                ng-click="state = states.aborted">Abort
        </button>
    </div>

    <!--    ng-style='{-->
    <!--    "padding-bottom":(100* maze.width/maze.width) + "%"-->
    <!--    }'-->

    <div id="mazeWrapper"

    >
        <canvas class="center-block" id="maze" width="720" height="720">

        </canvas>
    </div>


    <div class="page-header">
        <h2>
            Tiles
            <button type="button" class="btn btn-success" ng-click="addTileAction();">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </button>
        </h2>

    </div>

    <div class="edit-tile-wrapper" ng-hide="editTile == ''">
        <div>
            <div class="btn-group">
                <button type="button" class="btn "
                        ng-click="setDrawValue(1);"
                        ng-class='{
                            "btn-primary": drawValue==1,
                            "btn-default": drawValue!=1,
                        }'
                >
                    Wall
                </button>
                <button type="button" class="btn"
                        ng-click="setDrawValue(0);"
                        ng-class='{
                            "btn-primary": drawValue==0,
                            "btn-default": drawValue!=0,
                        }'
                >
                    Path
                </button>
            </div>

            <div class="btn-group pull-right">
                <button type="button" class="btn btn-danger" ng-click="editTile= ''">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
                <button type="button" class="btn "
                        ng-click="tileRotation = !tileRotation;"
                        ng-class='{
                            "btn-primary": tileRotation,
                            "btn-default": !tileRotation,
                        }'
                >
                    Rotational
                </button>
                <button type="button" class="btn "
                        ng-click="tileReflection = !tileReflection;"
                        ng-class='{
                            "btn-primary": tileReflection,
                            "btn-default": !tileReflection,
                        }'
                >
                    Reflection
                </button>
                <button type="button" class="btn btn-success" ng-click="saveTileAction()">
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                </button>
            </div>
        </div>

        <div
            ng-style='{
                    "height":(tiles.height*tileSize) + "px",
                    "width":(tiles.width*tileSize) + "px"
                }'
            class="tile"
        >
            <div class="tile-content">
                <div class="clearfix" ng-repeat="tileRow in tileDraft track by $index">
                    <div
                        ondragstart="return false;" ondrop="return false;"
                        ng-mousemove="drawTile($event,$parent.$index,$index);"
                        ng-style='{
                            "width": tileSize+"px",
                            "height": tileSize+"px",
                        }'
                        class="pull-left tile-part"
                        ng-repeat="c in tileRow track by $index"
                        ng-class='{
                            "tile-part-wall": c == 1,
                            "tile-part-path": c == 0,
                        }'
                    >

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix">
        <div
            class="pull-left"
            ng-repeat="(key, tile)  in tiles.data track by $index"
        >
            <div class="text-center">
                <!--                {{key}} <br>-->
                <div class="btn-group" ng-style='{"visibility":key != "tile_0" ? "visible": "hidden"}'>
                    <button type="button" class="btn btn-primary"
                            ng-click="editTileAction(key);"
                    >
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-danger"
                            ng-click="deleteTile(key);"
                    >
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </div>
            <div
                ng-style='{
                    "height":(tiles.height*tileSize) + "px",
                    "width":(tiles.width*tileSize) + "px"
                }'
                class="tile"
            >
                <div class="tile-content">
                    <div class="clearfix" ng-repeat="tileRow in tile track by $index">
                        <div
                            ng-style='{
                            "width": tileSize+"px",
                            "height": tileSize+"px",
                        }'
                            class="pull-left tile-part"
                            ng-repeat="c in tileRow track by $index"
                            ng-class='{
                            "tile-part-wall": c == 1,
                            "tile-part-path": c == 0,
                        }'
                        >

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</section>
