<?php

/* @var $this yii\web\View */

$this->title = 'Create New Maze';
?>

<div class="panel panel-default">
    <div class="panel-heading">Create New Maze</div>
    <div class="panel-body">
        <form action="/maze/create" method="post">
            <div class="form-group">
                <label for="maze_w">Maze Width</label>
                <input type="number" class="form-control" id="maze_w" name="maze_w" placeholder="Maze Width" value="20">
            </div>
            <div class="form-group">
                <label for="maze_h">Maze Height</label>
                <input type="number" class="form-control" id="maze_h" name="maze_h" placeholder="Maze Height"
                       value="20">
            </div>

            <div class="form-group">
                <label for="tile_w">Tile Width</label>
                <input type="number" class="form-control" id="tile_w" name="tile_w" placeholder="Tile Width" value="3">
            </div>
            <div class="form-group">
                <label for="tile_h">Tile Height</label>
                <input type="number" class="form-control" id="tile_h" name="tile_h" placeholder="Tile Height" value="3">
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" id="private" name="private" value="1"> Private Maze
                </label>
            </div>
            <button type="submit" class="btn btn-default">Create</button>

            <input id="form-token" type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                   value="<?= Yii::$app->request->csrfToken ?>"/>
        </form>

    </div>
</div>

