<?php

use common\models\Maze;

$mazes = Maze::find()
    ->select([
        "public_id",
        "last_generated",
    ])
    ->where([
        "private" => 0,
    ])
    ->andWhere("last_generated IS NOT NULL")
    ->orderBy("last_generated DESC")
    ->limit(10)
    ->all();


/* @var $this yii\web\View */

$this->title = 'Mazinator';
?>

<div class="page-header">
    <h2>Latest Mazes</h2>
</div>


<?php foreach ($mazes as $maze) { ?>
    <h4><?= date("d F Y H:i:s", $maze->last_generated) ?></h4>
    <a href="/maze/<?= $maze->public_id ?>">
        <img class="center-block" src="/img/maze/<?= $maze->public_id ?>.png" style="max-width: 100%;">
    </a>
<?php } ?>
