<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
?>


<div class="page-header">
    <h2>About</h2>
</div>

<h3>
    Mazinator is intended to be as comprehensive as possible, but if you found yourself lost, maybe these simple steps
    will halp you to get started.
</h3>

<h4>1. Main page contains latest public mazes for you to look at</h4>
<img class="center-block" src="/img/about/1.jpg" style="max-width:100%;">
<h4>2. Go to menu and click new maze. It will open window below. Configure and you are ready to go.</h4>
<img class="center-block" src="/img/about/2.jpg" style="max-width:100%;">
<h4>3. Click + button to add new tiles. Select wall or path and draw on the tile. Once finished click green button on
    the right. You can generate rotational and reflection tiles as well </h4>
<img class="center-block" src="/img/about/3.jpg" style="max-width:100%;">
<h4>4. Generate as many as you want. You can also edit and delete them.</h4>
<img class="center-block" src="/img/about/4.jpg" style="max-width:100%;">
<h4>5. Once that done, click generate. If tiles are sufficient for a maze, it will be ready in no time. Solve button
    allows to show how to solve it and export will let you download text representation of maze.</h4>
<img class="center-block" src="/img/about/5.jpg" style="max-width:100%;">
<h4>6. If generation is taking too long, you can click abort and ajust tiles/maze size</h4>
<img class="center-block" src="/img/about/6.jpg" style="max-width:100%;">
<h2>7. Enjoy mazinator</h2>



