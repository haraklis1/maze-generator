<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MazeExternalAsset extends AssetBundle
{
    public $sourcePath = '@bower';
    public $css = [
        "bootstrap/dist/css/bootstrap.min.css",
    ];
    public $js = [
        "jquery/dist/jquery.min.js",
        "bootstrap/dist/js/bootstrap.min.js",
        "angular/angular.min.js",
    ];
    public $depends = [
    ];
}
