<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/maze.css',
    ];
    public $js = [
        'js/maze/Blob.js',
        'js/maze/FileSaver.min.js',
        'js/maze/main.js',
        'js/maze/controllers/MazeController.js',
    ];
    public $depends = [
        'frontend\assets\MazeExternalAsset',
    ];
}
