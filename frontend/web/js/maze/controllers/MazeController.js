document.onmousedown = mouseDownEvent;
document.onmouseup = mouseUpEvent;
var mouseDown = false;

function mouseDownEvent(ev) {
    if (ev.which == 1) {
        mouseDown = true;
    }

}

function mouseUpEvent(ev) {
    if (ev.which == 1) {
        mouseDown = false;
    }
}

mzntr.controller("MazeController", function ($scope, $http, $attrs) {

    $scope.publicId = $attrs.publicId;
    $scope.maze = {};
    $scope.tiles = {};
    var tilesMetaData = {};
    var specialTiles = {};
    $scope.editTile = '';
    $scope.tileSize = 40;
    $scope.drawValue = 1;
    $scope.tileDraft = [];

    $scope.tileReflection = true;
    $scope.tileRotation = true;
    $scope.states = {
        aborted: "aborted",
        generating: "generating",
        ready: "ready",
        error: "error",
        success: "success",
    };
    $scope.state = $scope.states.ready;


    var colorWall = "#008000";
    var colorPath = "#FFFFFF";
    var colorSolution = "#00FF00";
    var newTileId = -1;

    $scope.export = function () {
        var text = "";
        var data = convertToSimpleForm();

        var h = data.length;
        var w = data[0].length;

        text += w + " " + h + "\n";

        for (var i = 0; i < data.length; i++) {
            for (var ii = 0; ii < data[i].length; ii++) {
                text += data[i][ii];
            }
            text += "\n";
        }

        var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
        saveAs(blob, "maze.txt");
    }

    var getMazeData = function () {
        $http.get("/maze/data/" + $scope.publicId).success(function (data) {
            $scope.maze = data.maze;

            $scope.newWidth = $scope.maze.width;
            $scope.newHeight = $scope.maze.height;

            $scope.tiles = data.tiles;
            $scope.redrawMaze();

            $scope.state = $scope.states.success;
        });
    };

    $scope.redrawMaze = function () {
        clearMaze(false);
        updateTilesMetaData();

        // console.log(tilesMetaData);

        var canvas = document.getElementById('maze');
        var context = canvas.getContext('2d');

        var tileWidth = canvas.width / $scope.maze.width;
        var subTileSize = tileWidth / $scope.tiles.width;
        var tileHeight = subTileSize * $scope.tiles.height;
        for (var i in $scope.maze.data) {
            for (var ii in $scope.maze.data[i]) {
                drawTileOnMaze(context, ii, i, tileWidth, tileHeight);
            }
        }
    };

    getMazeData();

    var updateTilesMetaData = function () {
        var canvas = document.getElementById('maze');

        var tileWidth = canvas.width / $scope.maze.width;
        var subTileSize = tileWidth / $scope.tiles.width;

        canvas.height = $scope.maze.height * $scope.tiles.height * subTileSize;

        var verticalWall = "";
        var horizontalWall = "";
        for (var j = 0; j < $scope.tiles.height; j++) {
            verticalWall += "1";
        }

        for (var j = 0; j < $scope.tiles.width; j++) {
            horizontalWall += "1";
        }

        tilesMetaData = {};

        var maxExitCount = 0;

        for (i in $scope.tiles.data) {
            var tileCanvas = document.createElement("canvas");
            tileCanvas.width = subTileSize * $scope.tiles.width;
            tileCanvas.height = subTileSize * $scope.tiles.height;
            var ctx = tileCanvas.getContext('2d');

            var data = [];

            for (var j in  $scope.tiles.data[i]) {
                var temp = [];
                for (var jj in  $scope.tiles.data[i][j]) {
                    temp.push($scope.tiles.data[i][j][jj] - 1);

                    ctx.fillStyle = $scope.tiles.data[i][j][jj] == 1 ?
                        colorWall : colorPath;
                    ctx.fillRect(
                        parseInt(jj) * subTileSize,
                        parseInt(j) * subTileSize,
                        subTileSize,
                        subTileSize
                    );

                    // ctx.fill();
                }
                data.push(temp);
            }
            var exit = 1;
            for (var j in  data) {
                for (var jj in  data[j]) {
                    if (data[j][jj] == -1) {
                        var q = [{x: parseInt(jj), y: parseInt(j)}];
                        while (q.length > 0) {
                            var pos = q.shift();
                            data[pos.y][pos.x] = exit;

                            if (
                                pos.y > 0 &&
                                data[pos.y - 1][pos.x] == -1
                            ) {
                                q.push({x: pos.x, y: pos.y - 1});
                            }
                            if (
                                pos.y < $scope.tiles.height - 1 &&
                                data[pos.y + 1][pos.x] == -1
                            ) {
                                q.push({x: pos.x, y: pos.y + 1});
                            }
                            if (
                                pos.x > 0 &&
                                data[pos.y][pos.x - 1] == -1
                            ) {
                                q.push({x: pos.x - 1, y: pos.y});
                            }
                            if (
                                pos.x < $scope.tiles.width - 1 &&
                                data[pos.y][pos.x + 1] == -1
                            ) {
                                q.push({x: pos.x + 1, y: pos.y});
                            }
                        }

                        exit++;
                    }
                }
            }

            var leftBorder = "";
            var rightBorder = "";
            var topBorder = "";
            var bottomBorder = "";

            var leftExit = [];
            var rightExit = [];
            var topExit = [];
            var bottomExit = [];
            for (var j = 0; j < $scope.tiles.height; j++) {
                leftBorder += $scope.tiles.data[i][j][0];
                rightBorder += $scope.tiles.data[i][j][$scope.tiles.width - 1];

                leftExit.push(data[j][0]);
                rightExit.push(data[j][$scope.tiles.width - 1]);
            }

            for (var j = 0; j < $scope.tiles.width; j++) {
                topBorder += $scope.tiles.data[i][0][j];
                bottomBorder += $scope.tiles.data[i][$scope.tiles.height - 1][j];

                topExit.push(data[0][j]);
                bottomExit.push(data[$scope.tiles.height - 1][j]);
            }

            tilesMetaData[i] = {
                canvas: tileCanvas,
                ctx: ctx,
                img: convertCanvasToImage(tileCanvas),
                border: {
                    top: topBorder,
                    right: rightBorder,
                    bottom: bottomBorder,
                    left: leftBorder
                },
                wall: {
                    top: topBorder == horizontalWall,
                    right: rightBorder == verticalWall,
                    bottom: bottomBorder == horizontalWall,
                    left: leftBorder == verticalWall
                },
                exitCount: exit - 1,
                priority: 1,
                exit: {
                    top: topExit,
                    right: rightExit,
                    bottom: bottomExit,
                    left: leftExit
                },
                matchingTiles: {
                    top: [],
                    right: [],
                    bottom: [],
                    left: []
                }
            };

            maxExitCount = Math.max(maxExitCount, exit - 1);

            for (j in $scope.tiles.data) {

                if (
                    tilesMetaData[i].border.bottom != horizontalWall &&
                    tilesMetaData[j].border.top == tilesMetaData[i].border.bottom) {
                    tilesMetaData[j].matchingTiles.top.push(i);
                    tilesMetaData[i].matchingTiles.bottom.push(j);
                }

                if (
                    tilesMetaData[i].border.top != horizontalWall &&
                    tilesMetaData[j].border.bottom == tilesMetaData[i].border.top) {
                    tilesMetaData[j].matchingTiles.bottom.push(i);
                    tilesMetaData[i].matchingTiles.top.push(j);
                }

                if (
                    tilesMetaData[i].border.left != verticalWall &&
                    tilesMetaData[j].border.right == tilesMetaData[i].border.left) {
                    tilesMetaData[j].matchingTiles.right.push(i);
                    tilesMetaData[i].matchingTiles.left.push(j);
                }

                if (
                    tilesMetaData[i].border.right != verticalWall &&
                    tilesMetaData[j].border.left == tilesMetaData[i].border.right) {
                    tilesMetaData[j].matchingTiles.left.push(i);
                    tilesMetaData[i].matchingTiles.right.push(j);
                }
                if (j == i) {
                    break;
                }
            }
        }

        for (i in tilesMetaData) {
            tilesMetaData[i].priority = (maxExitCount - tilesMetaData[i].exitCount + 1) * 10;
        }

        specialTiles = {};
        var tileCanvasError = document.createElement("canvas");
        tileCanvasError.width = subTileSize * $scope.tiles.width;
        tileCanvasError.height = subTileSize * $scope.tiles.height;
        var ctxError = tileCanvasError.getContext('2d');
        ctxError.fillStyle = "red";
        ctxError.fillRect(0, 0, tileCanvasError.width, tileCanvasError.height);
        specialTiles["tile_error"] = {
            canvas: tileCanvasError,
            ctx: ctxError,
            img: convertCanvasToImage(tileCanvasError),
        }

        var tileCanvasError = document.createElement("canvas");
        tileCanvasError.width = subTileSize * $scope.tiles.width;
        tileCanvasError.height = subTileSize * $scope.tiles.height;
        var ctxError = tileCanvasError.getContext('2d');
        ctxError.fillStyle = "red";
        ctxError.fillRect(0, 0, tileCanvasError.width, tileCanvasError.height);
        specialTiles["tile_error"] = {
            canvas: tileCanvasError,
            ctx: ctxError,
            img: convertCanvasToImage(tileCanvasError),
        }
    };

    var drawTileOnMaze = function (context, x, y, tileWidth, tileHeight) {
        // console.log("Draw: " + "tile_" + $scope.maze.data[y][x]);
        context.drawImage(
            ("tile_" + $scope.maze.data[y][x]) in tilesMetaData ?
                tilesMetaData["tile_" + $scope.maze.data[y][x]].canvas :
                tilesMetaData["tile_0"].canvas,
            parseInt(x) * tileWidth,
            parseInt(y) * tileHeight,
            tileWidth,
            tileHeight
        )
    };

    var drawSpecialTileOnMaze = function (context, x, y, tileWidth, tileHeight, specialType) {
        switch (specialType) {
            case "error":
                context.fillStyle = "red";
                break;
            case "warning":
                context.fillStyle = "yellow";
                break;
            default:
                context.fillStyle = "white";
                break;
        }

        context.fillRect(parseInt(x) * tileWidth, parseInt(y) * tileHeight, tileWidth, tileHeight);
    };

    var clearMaze = function (dataClear) {
        var canvas = document.getElementById('maze');
        var context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
        if (dataClear) {
            for (var i in $scope.maze.data) {
                for (var ii in $scope.maze.data[i]) {
                    $scope.maze.data[i][ii] = 0;
                }
            }
        }
    };

    var resizeMaze = function () {
        $scope.maze.width = parseInt($scope.newWidth);
        $scope.maze.height = parseInt($scope.newHeight);
        $scope.maze.data = [];
        for (var i = 0; i < $scope.maze.height; i++) {
            $scope.maze.data.push([]);
            for (var ii = 0; ii < $scope.maze.width; ii++) {
                $scope.maze.data[i].push(0);
            }
        }
    };

    $scope.generateMaze = function (cleanRun) {
        if ($scope.newWidth != $scope.maze.width || $scope.newHeight != $scope.maze.height) {
            resizeMaze();
        }

        updateTilesMetaData();

        Math.seed = 6;

        if (cleanRun)
            clearMaze(true);
        var canvas = document.getElementById('maze');
        var context = canvas.getContext('2d');

        var tileWidth = canvas.width / $scope.maze.width;
        var subTileSize = tileWidth / $scope.tiles.width;
        var tileHeight = subTileSize * $scope.tiles.height;


        $scope.state = $scope.states.generating;

        var q = [];
        var errorQ = [];
        if (cleanRun) {
            q.push({x: 0, y: 0})
        } else {
            for (var i in $scope.maze.data) {
                for (var ii in $scope.maze.data[i]) {
                    if ($scope.maze.data[i][ii] == 0) {
                        q.push({x: parseInt(ii), y: parseInt(i)});
                    }
                }
            }
        }

        var visited = [];
        var temp = [];
        for (var i in $scope.maze.data) {
            temp = [];
            for (var ii in $scope.maze.data[i]) {
                temp.push(false);
            }
            visited.push(temp);
        }

        var tilesPriorities = [];
        var tiles = [];
        for (var i in tilesMetaData) {
            if (i == "tile_0")
                continue;
            tilesPriorities.push({
                val: i,
                priority: tilesMetaData[i].priority
            });
            tiles.push(i);
        }

        var stage = 0;


        var step = function () {
            if ($scope.state != $scope.states.generating)
                return;

            if (q.length > 0) {
                var pos = q.pop();
                // visited[pos.y][pos.x] = true;
                if ($scope.maze.data[pos.y][pos.x] == 0) {
                    // console.log(pos);
                    // tiles = shuffleWithPriority(tilesPriorities);
                    shuffle(tiles);
                    var foundMatch = false;
                    for (i in tiles) {
                        if (tileFits(pos, tiles[i])) {
                            var temp = tilesMetaData[tiles[i]];
                            foundMatch = true;
                            $scope.maze.data[pos.y][pos.x] = parseInt(tiles[i].substring("tile_".length));
                            drawTileOnMaze(context, pos.x, pos.y, tileWidth, tileHeight);
                            //top
                            if (
                                pos.y > 0 &&
                                $scope.maze.data[pos.y - 1][pos.x] == 0 &&
                                visited[pos.y - 1][pos.x] == false &&
                                temp.matchingTiles.top.length > 0
                            ) {
                                q.push({x: pos.x, y: pos.y - 1});
                                visited[pos.y - 1][pos.x] = true;
                            }
                            //bottom
                            if (
                                pos.y < $scope.maze.height - 1 &&
                                $scope.maze.data[pos.y + 1][pos.x] == 0 &&
                                visited[pos.y + 1][pos.x] == false &&
                                temp.matchingTiles.bottom.length > 0
                            ) {
                                q.push({x: pos.x, y: pos.y + 1});
                                visited[pos.y + 1][pos.x] = true;
                            }
                            //left
                            if (
                                pos.x > 0 &&
                                $scope.maze.data[pos.y][pos.x - 1] == 0 &&
                                visited[pos.y][pos.x - 1] == false &&
                                temp.matchingTiles.left.length > 0
                            ) {
                                q.push({x: pos.x - 1, y: pos.y});
                                visited[pos.y][pos.x - 1] = true;
                            }
                            //right
                            if (
                                pos.x < $scope.maze.width - 1 &&
                                $scope.maze.data[pos.y][pos.x + 1] == 0 &&
                                visited[pos.y][pos.x + 1] == false &&
                                temp.matchingTiles.right.length > 0
                            ) {
                                q.push({x: pos.x + 1, y: pos.y});
                                visited[pos.y][pos.x + 1] = true;
                            }
                            break;
                        }
                    }

                    if (!foundMatch) {
                        errorQ.push(pos);
                        drawSpecialTileOnMaze(context, pos.x, pos.y, tileWidth, tileHeight, "error");
                    }
                }
                setTimeout(step, 1);
            } else {
                stage++;
                if (stage == 1) {
                    q = errorQ.slice(0);
                    errorQ = [];
                    step();
                } else if (stage == 2) {
                    stepFixDeadEnd();
                } else {
                    $scope.state = $scope.states.ready;
                    $scope.$apply();
                    return;
                }
            }
        };

        var stepFixDeadEnd = function () {
            if ($scope.state != $scope.states.generating)
                return;
            if (errorQ.length > 0) {
                var pos = errorQ.shift();

                //top
                if (
                    pos.y > 0 &&
                    $scope.maze.data[pos.y - 1][pos.x] != 0
                    && !tilesMetaData["tile_" + $scope.maze.data[pos.y - 1][pos.x]].wall.bottom
                ) {
                    if (tileMakeDeadEndsToEmptySpace(pos.x, pos.y - 1, tiles)) {
                        drawTileOnMaze(context, pos.x, pos.y - 1, tileWidth, tileHeight);
                    } else {
                        drawSpecialTileOnMaze(context, pos.x, pos.y - 1, tileWidth, tileHeight, "error");
                    }
                }
                //bottom
                if (
                    pos.y < $scope.maze.height - 1 &&
                    $scope.maze.data[pos.y + 1][pos.x] != 0
                    && !tilesMetaData["tile_" + $scope.maze.data[pos.y + 1][pos.x]].wall.top
                ) {
                    if (tileMakeDeadEndsToEmptySpace(pos.x, pos.y + 1, tiles)) {
                        drawTileOnMaze(context, pos.x, pos.y + 1, tileWidth, tileHeight);
                    } else {
                        drawSpecialTileOnMaze(context, pos.x, pos.y + 1, tileWidth, tileHeight, "error");
                    }
                }
                //left
                if (
                    pos.x > 0 &&
                    $scope.maze.data[pos.y][pos.x - 1] != 0
                    && !tilesMetaData["tile_" + $scope.maze.data[pos.y][pos.x - 1]].wall.right
                ) {
                    if (tileMakeDeadEndsToEmptySpace(pos.x - 1, pos.y, tiles)) {
                        drawTileOnMaze(context, pos.x - 1, pos.y, tileWidth, tileHeight);
                    } else {
                        drawSpecialTileOnMaze(context, pos.x - 1, pos.y, tileWidth, tileHeight, "error");
                    }
                }
                //right
                if (
                    pos.x < $scope.maze.width - 1 &&
                    $scope.maze.data[pos.y][pos.x + 1] != 0
                    && !tilesMetaData["tile_" + $scope.maze.data[pos.y][pos.x + 1]].wall.left
                ) {
                    if (tileMakeDeadEndsToEmptySpace(pos.x + 1, pos.y, tiles)) {
                        drawTileOnMaze(context, pos.x + 1, pos.y, tileWidth, tileHeight);
                    } else {
                        drawSpecialTileOnMaze(context, pos.x + 1, pos.y, tileWidth, tileHeight, "error");
                    }
                }

                drawSpecialTileOnMaze(context, pos.x, pos.y, tileWidth, tileHeight, "warning");

                setTimeout(stepFixDeadEnd, 1);
            } else {
                stepMakeOpening();
            }
        };

        var skipOne = false;
        var stepMakeOpening = function () {
            if ($scope.state != $scope.states.generating)
                return;
            for (var i = 0; i < $scope.maze.data.length; i++) {
                for (var ii = 0; ii < $scope.maze.data[i].length; ii++) {
                    if ($scope.maze.data[i][ii] == 0) {
                        var pos = {x: parseInt(ii), y: parseInt(i)};
                        shuffle(tiles);
                        var foundMatch = false;
                        for (j in tiles) {

                            //top
                            if (
                                pos.y > 0 &&
                                $scope.maze.data[pos.y - 1][pos.x] != 0
                            ) {
                                if (tileFits({x: pos.x, y: pos.y - 1}, tiles[j], false)) {
                                    $scope.maze.data[pos.y - 1][pos.x] = parseInt(tiles[j].substring("tile_".length));
                                    drawTileOnMaze(context, pos.x, pos.y - 1, tileWidth, tileHeight);
                                    foundMatch = true;
                                    break;

                                }
                            }
                            //bottom
                            if (
                                pos.y < $scope.maze.height - 1 &&
                                $scope.maze.data[pos.y + 1][pos.x] != 0
                            ) {
                                if (tileFits({x: pos.x, y: pos.y + 1}, tiles[j], false)) {
                                    $scope.maze.data[pos.y + 1][pos.x] = parseInt(tiles[j].substring("tile_".length));
                                    drawTileOnMaze(context, pos.x, pos.y + 1, tileWidth, tileHeight);
                                    foundMatch = true;
                                    break;
                                }
                            }
                            //left
                            if (
                                pos.x > 0 &&
                                $scope.maze.data[pos.y][pos.x - 1] != 0
                            ) {
                                if (tileFits({x: pos.x - 1, y: pos.y}, tiles[j], false)) {
                                    $scope.maze.data[pos.y][pos.x - 1] = parseInt(tiles[j].substring("tile_".length));
                                    drawTileOnMaze(context, pos.x - 1, pos.y, tileWidth, tileHeight);
                                    foundMatch = true;
                                    break;
                                }
                            }
                            //right
                            if (
                                pos.x < $scope.maze.width - 1 &&
                                $scope.maze.data[pos.y][pos.x + 1] != 0
                            ) {
                                if (tileFits({x: pos.x + 1, y: pos.y}, tiles[j], false)) {
                                    $scope.maze.data[pos.y][pos.x + 1] = parseInt(tiles[j].substring("tile_".length));
                                    drawTileOnMaze(context, pos.x + 1, pos.y, tileWidth, tileHeight);
                                    foundMatch = true;
                                    break;
                                }
                            }
                        }
                        if (foundMatch) {
                            q.push(pos);
                            stage = 0;
                            step();
                            return;
                        } else {
                            drawSpecialTileOnMaze(context, pos.x, pos.y, tileWidth, tileHeight, "error");
                            $scope.state = $scope.states.error;
                            $scope.$apply();
                            alert("ERROR!");
                            return;
                        }
                    }

                }
            }
            updateMazeRemote();
            $scope.state = $scope.states.success;
            $scope.$apply();
            alert("FINISH!");
        };


        step();

        // console.log($scope.maze.data);
    };

    var tileMakeDeadEndsToEmptySpace = function (x, y, tiles) {
        shuffle(tiles);
        for (var i in tiles) {
            // if (tiles[i] == "tile_" + $scope.maze.data[y][x]) {
            //     continue;
            // }

            var tileExitMapping = {};
            var f = false;
            if (
                y > 0 &&
                $scope.maze.data[y - 1][x] != 0
                && !tilesMetaData["tile_" + $scope.maze.data[y - 1][x]].wall.bottom
            ) {
                if (tilesMetaData[tiles[i]].matchingTiles.top.indexOf("tile_" + $scope.maze.data[y - 1][x]) == -1)
                    continue;


                for (var j in tilesMetaData[tiles[i]].exit.top) {
                    if (tilesMetaData[tiles[i]].exit.top[j] + "" in tileExitMapping) {
                        if (tileExitMapping[tilesMetaData[tiles[i]].exit.top[j] + ""] !=
                            tilesMetaData["tile_" + $scope.maze.data[y][x]].exit.top[j]) {
                            f = true;
                            break;
                        }
                    } else {
                        tileExitMapping[tilesMetaData[tiles[i]].exit.top[j] + ""] =
                            tilesMetaData["tile_" + $scope.maze.data[y][x]].exit.top[j]
                    }
                }
                if (f) {
                    continue;
                }

            }
            else {
                if (!tilesMetaData[tiles[i]].wall.top)
                    continue;
            }
            //bottom
            if (
                y < $scope.maze.height - 1 &&
                $scope.maze.data[y + 1][x] != 0
                && !tilesMetaData["tile_" + $scope.maze.data[y + 1][x]].wall.top
            ) {
                if (tilesMetaData[tiles[i]].matchingTiles.bottom.indexOf("tile_" + $scope.maze.data[y + 1][x]) == -1)
                    continue;

                for (var j in tilesMetaData[tiles[i]].exit.bottom) {
                    if (tilesMetaData[tiles[i]].exit.bottom[j] + "" in tileExitMapping) {
                        if (tileExitMapping[tilesMetaData[tiles[i]].exit.bottom[j] + ""] !=
                            tilesMetaData["tile_" + $scope.maze.data[y][x]].exit.bottom[j]) {
                            f = true;
                            break;
                        }
                    } else {
                        tileExitMapping[tilesMetaData[tiles[i]].exit.bottom[j] + ""] =
                            tilesMetaData["tile_" + $scope.maze.data[y][x]].exit.bottom[j]
                    }
                }
                if (f) {
                    continue;
                }
            }
            else {
                if (!tilesMetaData[tiles[i]].wall.bottom)
                    continue;
            }
            //left
            if (
                x > 0 &&
                $scope.maze.data[y][x - 1] != 0
                && !tilesMetaData["tile_" + $scope.maze.data[y][x - 1]].wall.right

            ) {
                if (tilesMetaData[tiles[i]].matchingTiles.left.indexOf("tile_" + $scope.maze.data[y][x - 1]) == -1)
                    continue;

                for (var j in tilesMetaData[tiles[i]].exit.left) {
                    if (tilesMetaData[tiles[i]].exit.left[j] + "" in tileExitMapping) {
                        if (tileExitMapping[tilesMetaData[tiles[i]].exit.left[j] + ""] !=
                            tilesMetaData["tile_" + $scope.maze.data[y][x]].exit.left[j]) {
                            f = true;
                            break;
                        }
                    } else {
                        tileExitMapping[tilesMetaData[tiles[i]].exit.left[j] + ""] =
                            tilesMetaData["tile_" + $scope.maze.data[y][x]].exit.left[j]
                    }
                }
                if (f) {
                    continue;
                }
            }
            else {
                if (!tilesMetaData[tiles[i]].wall.left)
                    continue;
            }
            //right
            if (
                x < $scope.maze.width - 1 &&
                $scope.maze.data[y][x + 1] != 0
                && !tilesMetaData["tile_" + $scope.maze.data[y][x + 1]].wall.left
            ) {
                if (tilesMetaData[tiles[i]].matchingTiles.right.indexOf("tile_" + $scope.maze.data[y][x + 1]) == -1)
                    continue;

                for (var j in tilesMetaData[tiles[i]].exit.right) {
                    if (tilesMetaData[tiles[i]].exit.right[j] + "" in tileExitMapping) {
                        if (tileExitMapping[tilesMetaData[tiles[i]].exit.right[j] + ""] !=
                            tilesMetaData["tile_" + $scope.maze.data[y][x]].exit.right[j]) {
                            f = true;
                            break;
                        }
                    } else {
                        tileExitMapping[tilesMetaData[tiles[i]].exit.right[j] + ""] =
                            tilesMetaData["tile_" + $scope.maze.data[y][x]].exit.right[j]
                    }
                }
                if (f) {
                    continue;
                }
            }
            else {
                if (!tilesMetaData[tiles[i]].wall.right)
                    continue;
            }
            var temp = [];
            for (var j in tileExitMapping) {
                if (temp.indexOf(tileExitMapping[j]) == -1) {
                    temp.push(tileExitMapping[j]);
                } else {
                    f = true;
                }
            }

            if (f) {
                continue;
            }

            $scope.maze.data[y][x] = parseInt(tiles[i].substring("tile_".length));
            return true;
        }
        return false;
    };

    var tileFits = function (pos, tile, loopDetection) {
        loopDetection = typeof loopDetection !== 'undefined' ? loopDetection : true;

        //===================================================================
        //====If it is a border of maze but tile doesn't have wall there=====
        //===================================================================
        if (pos.x == 0 && !tilesMetaData[tile].wall.left) {
            return false;
        }
        if (pos.y == 0 && !tilesMetaData[tile].wall.top) {
            return false;
        }
        if (pos.x == $scope.maze.width - 1 && !tilesMetaData[tile].wall.right) {
            return false;
        }
        if (pos.y == $scope.maze.height - 1 && !tilesMetaData[tile].wall.bottom) {
            return false;
        }
        //===================================================================
        //====Tile wall/non wall mismatch====================================
        //===================================================================

        if (
            pos.y > 0 &&
            $scope.maze.data[pos.y - 1][pos.x] != 0 && !xor(tilesMetaData["tile_" + $scope.maze.data[pos.y - 1][pos.x]].wall.bottom,
                tilesMetaData[tile].wall.top == false)
        ) {
            return false;
        }

        if (
            pos.y < $scope.maze.height - 1 &&
            $scope.maze.data[pos.y + 1][pos.x] != 0 && !xor(tilesMetaData["tile_" + $scope.maze.data[pos.y + 1][pos.x]].wall.top,
                tilesMetaData[tile].wall.bottom == false)
        ) {
            return false;
        }

        if (
            pos.x > 0 &&
            $scope.maze.data[pos.y][pos.x - 1] != 0 && !xor(tilesMetaData["tile_" + $scope.maze.data[pos.y][pos.x - 1]].wall.right,
                tilesMetaData[tile].wall.left == false)
        ) {
            return false;
        }

        if (
            pos.x < $scope.maze.width - 1 &&
            $scope.maze.data[pos.y][pos.x + 1] != 0 && !xor(tilesMetaData["tile_" + $scope.maze.data[pos.y][pos.x + 1]].wall.left,
                tilesMetaData[tile].wall.right == false)
        ) {
            return false;
        }
        //===================================================================
        //====tiles are incompatible ========================================
        //===================================================================

        if (
            pos.y > 0 &&
            $scope.maze.data[pos.y - 1][pos.x] != 0 &&
            tilesMetaData["tile_" + $scope.maze.data[pos.y - 1][pos.x]].wall.bottom == false &&
            tilesMetaData[tile].matchingTiles.top.indexOf("tile_" + $scope.maze.data[pos.y - 1][pos.x]) == -1

        ) {
            return false;
        }

        if (
            pos.y < $scope.maze.height - 1 &&
            $scope.maze.data[pos.y + 1][pos.x] != 0 &&
            tilesMetaData["tile_" + $scope.maze.data[pos.y + 1][pos.x]].wall.top == false &&
            tilesMetaData[tile].matchingTiles.bottom.indexOf("tile_" + $scope.maze.data[pos.y + 1][pos.x]) == -1
        ) {
            return false;
        }

        if (
            pos.x > 0 &&
            $scope.maze.data[pos.y][pos.x - 1] != 0 &&
            tilesMetaData["tile_" + $scope.maze.data[pos.y][pos.x - 1]].wall.right == false &&
            tilesMetaData[tile].matchingTiles.left.indexOf("tile_" + $scope.maze.data[pos.y][pos.x - 1]) == -1
        ) {
            return false;
        }

        if (
            pos.x < $scope.maze.width - 1 &&
            $scope.maze.data[pos.y][pos.x + 1] != 0 &&
            tilesMetaData["tile_" + $scope.maze.data[pos.y][pos.x + 1]].wall.left == false &&
            tilesMetaData[tile].matchingTiles.right.indexOf("tile_" + $scope.maze.data[pos.y][pos.x + 1]) == -1
        ) {
            return false;
        }

        if (loopDetection) {
            //===================================================================
            //==Make sure tile does not create a loop============================
            //===================================================================
            var exits = [];
            if (pos.x < $scope.maze.width - 1 &&
                $scope.maze.data[pos.y][pos.x + 1] != 0) {

                for (var i = 0; i < $scope.tiles.height; i++) {
                    if (tilesMetaData["tile_" + $scope.maze.data[pos.y][pos.x + 1]].exit.left[i] == 0)
                        continue;

                    if (exits.indexOf(tilesMetaData[tile].exit.right[i]) == -1
                    ) {
                        exits.push(tilesMetaData[tile].exit.right[i]);
                    } else {
                        return false;
                    }
                }

            }
            if (pos.x > 0 &&
                $scope.maze.data[pos.y][pos.x - 1] != 0) {
                for (var i = 0; i < $scope.tiles.height; i++) {
                    if (tilesMetaData["tile_" + $scope.maze.data[pos.y][pos.x - 1]].exit.right[i] == 0)
                        continue;
                    if (
                        exits.indexOf(tilesMetaData[tile].exit.left[i]) == -1
                    ) {
                        exits.push(tilesMetaData[tile].exit.left[i]);
                    } else {
                        return false;
                    }
                }
            }
            if (pos.y < $scope.maze.height - 1 &&
                $scope.maze.data[pos.y + 1][pos.x] != 0) {
                for (var i = 0; i < $scope.tiles.width; i++) {
                    if (tilesMetaData["tile_" + $scope.maze.data[pos.y + 1][pos.x]].exit.top[i] == 0)
                        continue;
                    if (exits.indexOf(tilesMetaData[tile].exit.bottom[i]) == -1
                    ) {
                        exits.push(tilesMetaData[tile].exit.bottom[i]);
                    } else {
                        return false;
                    }
                }
            }
            if (pos.y > 0 &&
                $scope.maze.data[pos.y - 1][pos.x] != 0) {
                for (var i = 0; i < $scope.tiles.width; i++) {
                    if (tilesMetaData["tile_" + $scope.maze.data[pos.y - 1][pos.x]].exit.bottom[i] == 0)
                        continue;
                    if (exits.indexOf(tilesMetaData[tile].exit.top[i]) == -1
                    ) {
                        exits.push(tilesMetaData[tile].exit.top[i]);
                    } else {
                        return false;
                    }
                }
            }
        }
        //===================================================================
        //==If there are free spaces, do not use dead ends===================
        //===================================================================
        var existsToExtension = [];
        var freeNeighbours = 0;
        if (pos.x < $scope.maze.width - 1 &&
            $scope.maze.data[pos.y][pos.x + 1] == 0) {
            freeNeighbours++;
            for (var i = 0; i < $scope.tiles.height; i++) {
                if (tilesMetaData[tile].exit.right[i] == 0)
                    continue;

                if (existsToExtension.indexOf(tilesMetaData[tile].exit.right[i]) == -1) {
                    existsToExtension.push(tilesMetaData[tile].exit.right[i]);
                }
            }
        }
        if (pos.x > 0 &&
            $scope.maze.data[pos.y][pos.x - 1] == 0) {
            freeNeighbours++;
            for (var i = 0; i < $scope.tiles.height; i++) {
                if (tilesMetaData[tile].exit.left[i] == 0)
                    continue;

                if (existsToExtension.indexOf(tilesMetaData[tile].exit.left[i]) == -1) {
                    existsToExtension.push(tilesMetaData[tile].exit.left[i]);
                }
            }
        }
        if (pos.y < $scope.maze.height - 1 &&
            $scope.maze.data[pos.y + 1][pos.x] == 0) {
            freeNeighbours++;
            for (var i = 0; i < $scope.tiles.width; i++) {
                if (tilesMetaData[tile].exit.bottom[i] == 0)
                    continue;

                if (existsToExtension.indexOf(tilesMetaData[tile].exit.bottom[i]) == -1) {
                    existsToExtension.push(tilesMetaData[tile].exit.bottom[i]);
                }
            }
        }
        if (pos.y > 0 &&
            $scope.maze.data[pos.y - 1][pos.x] == 0) {
            freeNeighbours++;
            for (var i = 0; i < $scope.tiles.width; i++) {
                if (tilesMetaData[tile].exit.top[i] == 0)
                    continue;

                if (existsToExtension.indexOf(tilesMetaData[tile].exit.top[i]) == -1) {
                    existsToExtension.push(tilesMetaData[tile].exit.top[i]);
                }
            }
        }
        if (existsToExtension.length > 0) {
            var neighbours = 0;

            if (pos.x < $scope.maze.width - 1 &&
                $scope.maze.data[pos.y][pos.x + 1] != 0) {
                neighbours++;
                for (var i = 0; i < $scope.tiles.height; i++) {
                    if (tilesMetaData[tile].exit.right[i] == 0)
                        continue;
                    var e = existsToExtension.indexOf(tilesMetaData[tile].exit.right[i]);
                    if (e != -1) {
                        existsToExtension.splice(e, 1);
                    }
                }
            }
            if (pos.x > 0 &&
                $scope.maze.data[pos.y][pos.x - 1] != 0) {
                neighbours++;
                for (var i = 0; i < $scope.tiles.height; i++) {
                    if (tilesMetaData[tile].exit.left[i] == 0)
                        continue;

                    var e = existsToExtension.indexOf(tilesMetaData[tile].exit.left[i]);
                    if (e != -1) {
                        existsToExtension.splice(e, 1);
                    }
                }
            }
            if (pos.y < $scope.maze.height - 1 &&
                $scope.maze.data[pos.y + 1][pos.x] != 0) {
                neighbours++;
                for (var i = 0; i < $scope.tiles.width; i++) {
                    if (tilesMetaData[tile].exit.bottom[i] == 0)
                        continue;

                    var e = existsToExtension.indexOf(tilesMetaData[tile].exit.bottom[i]);
                    if (e != -1) {
                        existsToExtension.splice(e, 1);
                    }
                }
            }
            if (pos.y > 0 &&
                $scope.maze.data[pos.y - 1][pos.x] != 0) {
                neighbours++;
                for (var i = 0; i < $scope.tiles.width; i++) {
                    if (tilesMetaData[tile].exit.top[i] == 0)
                        continue;

                    var e = existsToExtension.indexOf(tilesMetaData[tile].exit.top[i]);
                    if (e != -1) {
                        existsToExtension.splice(e, 1);
                    }
                }
            }

            if (neighbours > 0 && existsToExtension.length != 0) {
                return false;
            }
        } else {
            if (freeNeighbours > 0) {
                return false;
            }
        }

        return true;
    };

    $scope.drawSolution = function () {
        var data = convertToSimpleForm();

        var h = data.length;
        var w = data[0].length;

        for (var i = 0; i < data.length; i++) {
            for (var ii = 0; ii < data[i].length; ii++) {
                if (data[i][ii] == 1) {
                    data[i][ii] = -1;
                }
            }
        }
        var start = getStartPosition(data);
        var q = [start];
        var end = getEndPosition(data);
        var found = false;

        while (q.length > 0) {
            var pos = q.shift();
            if (pos.x == end.x && pos.y == end.y) {
                found = true;
                break;
            }

            var parent = w * pos.y + pos.x;

            if (pos.x > 0
                && data[pos.y][pos.x - 1] == 0
            ) {
                data[pos.y][pos.x - 1] = parent;
                q.push({x: pos.x - 1, y: pos.y});
            }

            if (pos.x < w - 1
                && data[pos.y][pos.x + 1] == 0
            ) {
                data[pos.y][pos.x + 1] = parent;
                q.push({x: pos.x + 1, y: pos.y});
            }

            if (pos.y > 0
                && data[pos.y - 1][pos.x] == 0
            ) {
                data[pos.y - 1][pos.x] = parent;
                q.push({x: pos.x, y: pos.y - 1});
            }

            if (pos.y < h - 1
                && data[pos.y + 1][pos.x] == 0
            ) {
                data[pos.y + 1][pos.x] = parent;
                q.push({x: pos.x, y: pos.y + 1});
            }
        }

        if (found) {
            var canvas = document.getElementById('maze');
            var context = canvas.getContext('2d');

            var tileWidth = canvas.width / $scope.maze.width;
            var subTileSize = tileWidth / $scope.tiles.width;


            var x = end.x;
            var y = end.y;
            var temp;
            context.fillStyle = colorSolution;

            while (x != start.x || y != start.y) {
                context.fillRect(x * subTileSize, y * subTileSize, subTileSize, subTileSize);
                temp = data[y][x];
                x = temp % w;
                y = Math.floor(temp / w);
            }
            context.fillRect(x * subTileSize, y * subTileSize, subTileSize, subTileSize);
        } else {
            alert("Solution not found...");
        }

    };

    var getStartPosition = function (data) {
        var h = data.length;
        var w = data[0].length;
        var max = Math.max(w, h);

        for (var i = 0; i < max; i++) {
            for (ii = 0; ii < i; ii++) {
                if (i < h && ii < w) {
                    if (data[i][ii] == 0) {
                        return {x: ii, y: i};
                    }
                }
                if (i < w && ii < h) {
                    if (data[ii][i] == 0) {
                        return {x: i, y: ii};
                    }
                }
            }
            if (i < h && i < w) {
                if (data[i][i] == 0) {
                    return {x: i, y: i};
                }
            }
        }
    };

    var getEndPosition = function (data) {
        var h = data.length;
        var w = data[0].length;
        var max = Math.max(w, h);

        for (var i = 0; i < max; i++) {
            for (ii = 0; ii < i; ii++) {
                if (i < h && ii < w) {
                    if (data[h - 1 - i][w - 1 - ii] == 0) {
                        return {x: w - 1 - ii, y: h - 1 - i};
                    }
                }
                if (i < w && ii < h) {
                    if (data[h - 1 - ii][w - 1 - i] == 0) {
                        return {x: w - 1 - i, y: h - 1 - ii};
                    }
                }
            }
            if (i < h && i < w) {
                if (data[h - 1 - i][w - 1 - i] == 0) {
                    return {x: w - 1 - i, y: h - 1 - i};
                }
            }
        }
    };

    var convertToSimpleForm = function () {
        var data = [];

        for (var i = 0; i < $scope.maze.height * $scope.tiles.height; i++) {
            data.push([]);
        }

        for (var i in $scope.maze.data) {
            for (var ii in $scope.maze.data[i]) {
                var tile = $scope.tiles.data["tile_" + $scope.maze.data[i][ii]]
                for (var j in tile) {
                    for (var jj in tile[j]) {
                        data[parseInt(i) * $scope.tiles.height + parseInt(j)].push(tile[j][jj]);
                    }
                }
            }
        }
        return data;
    };

    var updateMazeRemote = function () {
        $.ajax({
            method: "POST",
            url: "/maze/update/" + $scope.publicId,
            dataType: "json",
            data: {
                maze: JSON.stringify($scope.maze.data)
            }
        }).done(function (data) {
            if (!data.success) {
                if (data.errors.length > 0) {
                    console.log(data.errors);
                }
            } else {
                console.log("send to remote success");
            }
        });
    };
//=========================TILE========================

    $scope.saveTileAction = function () {


        if (!($scope.editTile in $scope.tiles.data)) {
            newTileId--;
        }
        $scope.tiles.data[$scope.editTile] = $scope.tileDraft;
        $scope.editTile = '';

        if ($scope.tileReflection) {
            var horizontal = getEmptyTile();
            var vertical = getEmptyTile();
            for (i in $scope.tileDraft) {
                for (ii in $scope.tileDraft[i]) {
                    horizontal[i][$scope.tiles.width - (parseInt(ii) + 1)] = $scope.tileDraft[i][ii];
                    vertical[$scope.tiles.height - (parseInt(i) + 1)][ii] = $scope.tileDraft[i][ii];
                }
            }
            $scope.tiles.data["tile_" + newTileId] = horizontal;
            newTileId--;
            $scope.tiles.data["tile_" + newTileId] = vertical;
            newTileId--;
        }

        if ($scope.tileRotation) {
            if ($scope.tiles.width == $scope.tiles.height) {
                var rot90 = getEmptyTile();
                for (i in $scope.tileDraft) {
                    for (ii in $scope.tileDraft[i]) {
                        rot90[i][ii] = $scope.tileDraft[$scope.tiles.width - (parseInt(ii) + 1)][i];
                    }
                }
                $scope.tiles.data["tile_" + newTileId] = rot90;
                newTileId--;

                var rot180 = getEmptyTile();
                for (i in rot90) {
                    for (ii in rot90[i]) {
                        rot180[i][ii] = rot90[$scope.tiles.width - (parseInt(ii) + 1)][i];
                    }
                }
                $scope.tiles.data["tile_" + newTileId] = rot180;
                newTileId--;

                var rot270 = getEmptyTile();
                for (i in rot180) {
                    for (ii in rot180[i]) {
                        rot270[i][ii] = rot180[$scope.tiles.width - (parseInt(ii) + 1)][i];
                    }
                }
                $scope.tiles.data["tile_" + newTileId] = rot270;
                newTileId--;

            }
        }
        removeDuplicatedTiles();
        updateTilesRemote();
    };

    var updateTilesRemote = function () {
        $.ajax({
            method: "POST",
            url: "/maze/tiles/update/" + $scope.publicId,
            dataType: "json",
            data: {
                tiles: JSON.stringify($scope.tiles.data)
            }
        }).done(function (data) {
            for (i in data.tile_mappings) {
                $scope.tiles.data.renameProperty(i, data.tile_mappings[i]);
            }
            $scope.$apply();
            if (data.errors.length > 0) {
                console.log(data.errors);
            }
        });
    };

    var removeDuplicatedTiles = function () {
        var unique = [];
        for (var i in $scope.tiles.data) {
            var temp = JSON.stringify($scope.tiles.data[i]);
            if (unique.indexOf(temp) == -1) {
                unique.push(temp);
            } else {
                delete $scope.tiles.data[i];
            }
        }
    };

    $scope.editTileAction = function (tile) {
        $scope.editTile = tile;
        $scope.tileDraft = getEmptyTile();
        for (i in $scope.tiles.data[tile]) {
            for (ii in $scope.tiles.data[tile][i]) {
                $scope.tileDraft[i][ii] = $scope.tiles.data[tile][i][ii];
            }
        }
    };

    $scope.deleteTile = function (tile) {
        delete $scope.tiles.data[tile];
        updateTilesRemote();
    };

    $scope.setDrawValue = function (val) {
        $scope.drawValue = val;
    };

    $scope.drawTile = function (e, row, col) {
        if (mouseDown) {
            $scope.tileDraft[row][col] = $scope.drawValue;
        }
        // e.stopPropagation();
    };

    var getEmptyTile = function () {
        var tileData = [];
        for (var i = 0; i < $scope.tiles.height; i++) {
            var temp = [];
            for (var ii = 0; ii < $scope.tiles.width; ii++) {
                temp.push(0);
            }
            tileData.push(temp);
        }
        return tileData;
    };

    $scope.addTileAction = function () {
        $scope.editTileAction("tile_" + newTileId);
        // $scope.tiles.data["tile_" + newTileId] = getEmptyTile();
        // newTileId--;
    };


})
;

Object.defineProperty(
    Object.prototype,
    'renameProperty',
    {
        writable: false, // Cannot alter this property
        enumerable: false, // Will not show up in a for-in loop.
        configurable: false, // Cannot be deleted via the delete operator
        value: function (oldName, newName) {
            // Do nothing if the names are the same
            if (oldName == newName) {
                return this;
            }
            // Check for the old property name to
            // avoid a ReferenceError in strict mode.
            if (this.hasOwnProperty(oldName)) {
                this[newName] = this[oldName];
                delete this[oldName];
            }
            return this;
        }
    }
);

function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

function shuffleWithPriority(a) {
    var res = [], p = a.slice();
    var total = 0;
    for (var i in p) {
        total += p[i].priority;
    }
    var roll;
    var temp;
    for (var i in p) {
        roll = Math.floor(Math.random() * total);
        temp = 0;
        for (var ii in p) {
            temp += p[ii].priority;
            if (roll < temp) {
                res.push(p[ii].val);
                total -= p[ii].priority;
                p.splice(parseInt(ii), 1);
                break;
            }
        }
    }
    console.log(res);
    return res;
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function convertCanvasToImage(canvas) {
    var image = new Image();
    image.src = canvas.toDataURL("image/png");
    return image;
}

function xor(a, b) {
    return (a && !b ) || (!a && b );
}


// in order to work 'Math.seed' must NOT be undefined,
// so in any case, you HAVE to provide a Math.seed
Math.seededRandom = function (max, min) {
    max = max || 1;
    min = min || 0;

    Math.seed = (Math.seed * 9301 + 49297) % 233280;
    var rnd = Math.seed / 233280;

    return min + rnd * (max - min);
}