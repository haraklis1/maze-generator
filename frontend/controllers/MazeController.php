<?php
namespace frontend\controllers;

use common\models\Maze;
use common\models\MazeData;
use common\models\MazeTile;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Site controller
 */
class MazeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'view',
                            'get-maze-data',
                            'new',
                            'create',
                            'update-tiles',
                            'update-maze',
                            'generate',
                        ],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'create' => ['post'],
                    'update-tiles' => ['post'],
                    'update-maze' => ['post'],
                    'generate' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (
            $action->id == 'update-tiles'
            || $action->id == 'update-maze'
        ) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionView($publicId)
    {
        $maze = Maze::findOne(["public_id" => $publicId]);
        if ($maze) {
            return $this->render('view', [
                "publicId" => $publicId
            ]);
        }

        throw new \yii\web\NotFoundHttpException("Maze Not Found");
    }

    public function actionNew()
    {
        return $this->render('new');
    }

    public function actionCreate()
    {
        $maze = new Maze();
        $maze->public_id = $this->generateRandomString(16);
        $maze->maze_h = Yii::$app->request->post("maze_h", 20);
        $maze->maze_w = Yii::$app->request->post("maze_w", 20);
        $maze->tile_h = Yii::$app->request->post("tile_h", 3);
        $maze->tile_w = Yii::$app->request->post("tile_w", 3);
        $maze->private = Yii::$app->request->post("private", 0) == 1 ? 1 : 0;
        if (!$maze->save()) {
            throw new HttpException(500, print_r($maze->errors, true));
        }

        return $this->redirect("/maze/" . $maze->public_id);
    }

    public function actionGetMazeData($publicId)
    {
        $maze = Maze::findOne(["public_id" => $publicId]);
        if ($maze) {
            $tiles = MazeTile::find()
                ->select([
                    "id",
                    "data",
                ])->where([
                    "maze_id" => $maze->id
                ])->asArray()
                ->all();

            foreach ($tiles as $k => $tile) {
                $tiles[$k]["id"] = $tile["id"];
                $tiles[$k]["data"] = json_decode($tile["data"], true);
            }

            /** @var MazeData[] $mazeTiles */
            $mazeTiles = MazeData::find()
                ->select([
                    "id",
                    "maze_tile_id",
                    "x",
                    "y",
                ])->where([
                    "maze_id" => $maze->id
                ])
                ->all();

            /** @var MazeTile[] $allTiles */
            $allTiles = MazeTile::find()->select("id")->where(["maze_id" => $maze->id])->all();
            $allTilesIds = [];
            foreach ($allTiles as $t) {
                $allTilesIds[] = $t->id;
            }


            $mazeData = [];
            for ($i = 0; $i < $maze->maze_h; $i++) {
                $temp = [];
                for ($ii = 0; $ii < $maze->maze_w; $ii++) {
                    $temp[] = 0;//$allTilesIds[rand(0, count($allTilesIds) - 1)];
                }
                $mazeData[] = $temp;
            }

            foreach ($mazeTiles as $tile) {
                $mazeData[$tile->y][$tile->x] = $tile->maze_tile_id;
            }

            $tilesData = [];


            $tilesData["tile_0"] = [];

            for ($i = 0; $i < $maze->tile_h; $i++) {
                $temp = [];
                for ($ii = 0; $ii < $maze->tile_w; $ii++) {
                    $temp[] = 1;
                }
                $tilesData["tile_0"][] = $temp;
            }

            foreach ($tiles as $tile) {
                $tilesData["tile_" . $tile["id"]] = $tile["data"];
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                "maze" => [
                    "width" => $maze->maze_w,
                    "height" => $maze->maze_h,
                    "data" => $mazeData
                ],
                "tiles" => [
                    "width" => $maze->tile_w,
                    "height" => $maze->tile_h,
                    "data" => $tilesData
                ],
            ];
        }

        throw new \yii\web\NotFoundHttpException("Maze Not Found");
    }

    public function actionUpdateTiles($publicId)
    {
        $maze = Maze::findOne(["public_id" => $publicId]);
        if ($maze) {
            $errors = [];
            $tileMappings = [];
            $tiles = json_decode(Yii::$app->request->post("tiles", "[]"), true);

            /** @var MazeTile[] $allTiles */
            $allTiles = MazeTile::find()->select("id")->where(["maze_id" => $maze->id])->all();
            $allTilesIds = [];
            foreach ($allTiles as $t) {
                $allTilesIds[] = $t->id;
            }

            foreach ($tiles as $k => $tile) {
                $id = intval(substr($k, strlen("tile_")));
                if ($id == 0) {
                    continue;
                }

                if ($id < 0) {
                    $mazeTile = new MazeTile();
                    $mazeTile->maze_id = $maze->id;
                } else {
                    $mazeTile = MazeTile::findOne([
                        "id" => $id,
                        "maze_id" => $maze->id,
                    ]);
                    if (!$mazeTile)
                        continue;

                    if (($key = array_search($id, $allTilesIds)) !== false) {
                        unset($allTilesIds[$key]);
                    }

                }
                $mazeTile->data = json_encode($tile);
                if (!$mazeTile->save()) {
                    $errors[] = $mazeTile->errors;
                } else {
                    $tileMappings["tile_" . $id] = "tile_" . $mazeTile->id;
                }
            }

            MazeTile::deleteAll(["id" => $allTilesIds]);

            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                "tile_mappings" => count($tileMappings) > 0 ? $tileMappings : new \stdClass(),
                "errors" => $errors,
                "tiles" => $tiles,
                "req" => Yii::$app->request->post(),

            ];
        }

        throw new \yii\web\NotFoundHttpException("Maze Not Found");
    }

    public function actionUpdateMaze($publicId)
    {
        $maze = Maze::findOne(["public_id" => $publicId]);
        if ($maze) {
            $mazeData = json_decode(Yii::$app->request->post("maze", "[]"), true);
            $maze->maze_h = count($mazeData);
            $maze->maze_w = count($mazeData[0]);
            $maze->last_generated = time();

            MazeData::deleteAll(["maze_id" => $maze->id]);

            for ($i = 0; $i < $maze->maze_h; $i++) {
                for ($ii = 0; $ii < $maze->maze_w; $ii++) {
                    $temp = new MazeData();
                    $temp->maze_id = $maze->id;
                    $temp->x = $ii;
                    $temp->y = $i;
                    $temp->maze_tile_id = $mazeData[$i][$ii];
                    $temp->save(false);
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($maze->save()) {
                $data = [];

                $tiles = MazeTile::findAll(["maze_id" => $maze->id]);
                $tData = [];
                foreach ($tiles as $k => $v) {
                    $tData['tile_' . $v->id] = json_decode($v->data, true);
                }

                for ($i = 0; $i < $maze->maze_h * $maze->tile_h; $i++) {
                    $data[] = [];
                }

                for ($i = 0; $i < $maze->maze_h; $i++) {
                    for ($ii = 0; $ii < $maze->maze_w; $ii++) {
                        $tile = $tData['tile_' . $mazeData[$i][$ii]];
                        for ($j = 0; $j < $maze->tile_h; $j++) {
                            for ($jj = 0; $jj < $maze->tile_w; $jj++) {
                                $data[$i * $maze->tile_h + $j][] = $tile[$j][$jj];
                            }
                        }
                    }
                }

                $this->saveMazeToPicture($data, $publicId);

                return [
                    "success" => true
                ];
            } else {
                return [
                    "errors" => $maze->errors,
                    "success" => false
                ];
            }


        }

        throw new \yii\web\NotFoundHttpException("Maze Not Found");
    }

    private function saveMazeToPicture($data, $publicId)
    {
        $subTileSize = 4;
        $img = imagecreate(count($data[0]) * $subTileSize, count($data) * $subTileSize);

        $wall = imagecolorallocate($img, 0, 128, 0);
        $path = imagecolorallocate($img, 255, 255, 255);

        $str = "";

        for ($i = 0; $i < count($data); $i++) {
            for ($ii = 0; $ii < count($data[$i]); $ii++) {
                imagefilledrectangle($img,
                    $ii * $subTileSize,
                    $i * $subTileSize,
                    ($ii + 1) * $subTileSize,
                    ($i + 1) * $subTileSize,
                    $data[$i][$ii] == 1 ? $wall : $path
                );
                $str .= $data[$i][$ii];
            }
            $str .= "\n";
        }
        if (!is_dir('img/maze')) {
            mkdir('img/maze', 0777, true);
        }
        imagepng($img, "img/maze/" . $publicId . ".png");
    }

    public function actionGenerate($publicId)
    {
        $maze = Maze::findOne(["public_id" => $publicId]);
        if ($maze) {


            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                "public_id" => $publicId
            ];
        }

        throw new \yii\web\NotFoundHttpException("Maze Not Found");
    }

    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
